package com.example.android_homework_003;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class ScaleAnimation extends AppCompatActivity {
    ImageView imgScale;
    Animation scaleAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scale_animation);

        imgScale = findViewById(R.id.img_rabbit);
        scaleAnimation = AnimationUtils.loadAnimation(this,R.anim.animation_scale);
        imgScale.setAnimation(scaleAnimation);
    }
}
