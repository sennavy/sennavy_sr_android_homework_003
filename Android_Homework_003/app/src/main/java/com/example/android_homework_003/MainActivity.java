package com.example.android_homework_003;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    PopupWindow popupWindow;
    Button btnCancelNote;
    Button btnSaveNote;
    EditText title,content;
    private static int REQUEST_CODE = 1;
    LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        popupWindow = new PopupWindow(this);

        rootLayout = findViewById(R.id.main_layout);
    }
    private void cancelSave() {
        popupWindow.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.btnAddTask:
                item.setEnabled(false);
                popupFormModal(item);
                break;
            case R.id.op_remove:
                rootLayout.removeAllViews();
                Toast.makeText(this, "All content has been removed", Toast.LENGTH_SHORT).show();
                break;
            case R.id.translate_animation:
                openTranslateAnimation();
                break;
            case R.id.rotate_animation:
                openRotateAnimation();
                break;
            case R.id.scale_animation:
                openScaleAnimation();
                break;
            case R.id.fadeOut_animation:
                openFadeOut();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void openFadeOut() {
        Intent intent = new Intent(this, FadeAnimation.class);
        startActivity(intent);
    }

    private void openScaleAnimation() {
        Intent intent = new Intent(this, ScaleAnimation.class);
        startActivity(intent);
    }

    private void openTranslateAnimation() {
        Intent intent = new Intent(this,TranslateAnimation.class);
        startActivity(intent);
    }
    private void openRotateAnimation() {
        Intent intent = new Intent(this,RotateAnimation.class);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void popupFormModal(final MenuItem item){
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.popup_form, null);
        popupWindow = new PopupWindow(view,650, 650, true);
        popupWindow.setAnimationStyle(R.style.popup_animation);
        popupWindow.setTouchModal(false);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                item.setEnabled(true);
            }
        });

        btnCancelNote = view.findViewById(R.id.btnCancelNote);
        btnCancelNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        btnSaveNote = view.findViewById(R.id.btnSaveNote);
        title = view.findViewById(R.id.txtTitle);
        content = view.findViewById(R.id.txtContent);

        btnSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringTitle = title.getText().toString();
                String stringContent = content.getText().toString();
                if(stringTitle.isEmpty()){
                    Toast.makeText(MainActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                }
                else if(stringContent.isEmpty()){
                    Toast.makeText(MainActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                }
                else {
                    addTask(title.getText().toString(), content.getText().toString());
                    popupWindow.dismiss();
                }
            }
        });
    }

    private void addTask(String title, String content) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 20);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        linearLayout.setOrientation(LinearLayout.VERTICAL);


        TextView txtTitle = new TextView(this);
        txtTitle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        txtTitle.setEllipsize(TextUtils.TruncateAt.END);
        txtTitle.setLines(1);
        txtTitle.setTextSize(35);
        txtTitle.setPadding(20,10,15,10);
        txtTitle.setTypeface(null, Typeface.BOLD);

        TextView txtContent = new TextView(this);
        txtContent.setEllipsize(TextUtils.TruncateAt.END);
        txtContent.setLines(2);
        txtContent.setTextSize(15);
        txtContent.setPadding(20,10,15,10);

        linearLayout.addView(txtTitle);
        linearLayout.addView(txtContent);
        txtTitle.setText(title);
        txtContent.setText(content);

        rootLayout.addView(linearLayout);
        Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
    }

}
