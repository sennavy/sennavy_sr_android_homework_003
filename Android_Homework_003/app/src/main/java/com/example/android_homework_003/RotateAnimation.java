package com.example.android_homework_003;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class RotateAnimation extends AppCompatActivity {
    Animation rotateAnimation;
    ImageView imgRotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotate_animation);

        imgRotate = findViewById(R.id.img_earth);
        rotateAnimation = AnimationUtils.loadAnimation(this,R.anim.animation_rotate_earth);
        imgRotate.setAnimation(rotateAnimation);
    }
}
