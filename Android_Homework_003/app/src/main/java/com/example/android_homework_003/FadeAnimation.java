package com.example.android_homework_003;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class FadeAnimation extends AppCompatActivity {
    ImageView imgfadeIn;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fade_animation);

        imgfadeIn = findViewById(R.id.fadIn);
        animation = AnimationUtils.loadAnimation(this,R.anim.fadein);
        imgfadeIn.setAnimation(animation);
    }
}
