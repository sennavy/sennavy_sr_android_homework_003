package com.example.android_homework_003;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class TranslateAnimation extends AppCompatActivity {
    ImageView imgGhost,imgCat;
    Animation ghostAnimation;
    Animation catAnimation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translate__animation);

        imgGhost = findViewById(R.id.ghost_walking);
        ghostAnimation = AnimationUtils.loadAnimation(TranslateAnimation.this,R.anim.animation_translate_img_ghost);
        imgGhost.startAnimation(ghostAnimation);

        imgCat = findViewById(R.id.cat_running);
        catAnimation = AnimationUtils.loadAnimation(TranslateAnimation.this,R.anim.animation_translate_img_cat);
        imgCat.startAnimation(catAnimation);




    }
}
